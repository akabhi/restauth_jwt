# Django REST API Auth using_JWT

Django REST API authorization using JWT

use any 1 of them for JWTauth -
1. djangorestframework-jwt (used in this project)
2. django-rest-framework-simplejwt

add in settings.py -

    JWT_AUTH = {
    'JWT_ALLOW_REFRESH': True,
    }

to get token from cmd-

    http http://127.0.0.1:8000/auth-jwt/ username="durga" password="password"

to get token from postman-
POST request to http://127.0.0.1:8000/auth-jwt/
in body enter: username and password

to verify token from cmd

    http POST http://127.0.0.1:8000/auth-jwt-verify/ token="generated-token"

to verify from postman

    POST request to http://127.0.0.1:8000/auth-jwt-verify/
    in body enter: token

to refresh token from cmd

    http http://127.0.0.1:8000/auth-jwt-refresh/ token="token"

to refresh from postman

    POST request to http://127.0.0.1:8000/auth-jwt-refresh
    in body enter: token
