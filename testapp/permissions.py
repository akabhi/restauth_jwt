
from rest_framework.permissions import BasePermission, SAFE_METHODS

# custom permission to allow SAFE method only e.g. GET, HEAD, OPTIONS
class IsReadOnly(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        else:
            return False

# custom class for GET and PATCH only
class IsGETorPATCH(BasePermission):
    def has_permission(self, request, view):
        allowed_methods = ['GET', 'PATCH']
        if request.method in allowed_methods:
            return True
        else:
            return False

# if name is sunny then allow all methods
# if the name is not sunny and name contains even number of characters then all safe methods are allowed
# otherwise not allowed to perform any operation
class SunnyPermission(BaseException):
    def has_permission(self, request, view):
        username = request.user.username # if user is not authenticated the empty string is returned not None
        if username.lower() == 'sunny':
            return True
        elif username != '' and len(username)%2 == 0 and request.method in SAFE_METHODS:
            return True
        else:
            return False

# in the tutorial it wasn't necesaary to add has_object_permission method
# but its asking to add this method for object level access localhost:8000/1/
# it might be issue related to version or some settings related problem
    def has_object_permission(self, request, view, obj):
        username = request.user.username # if user is not authenticated the empty string is returned not None
        if username.lower() == 'sunny':
            return True
        elif username != '' and len(username)%2 == 0 and request.method in SAFE_METHODS:
            return True
        else:
            return False
