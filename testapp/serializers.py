from testapp.models import Employee
from rest_framework.serializers import ModelSerializer

class EmployeeSerializer(ModelSerializer):
    '''
    all kind of validations are performed here
    same in APIView and ViewsSet
    '''
    class Meta:
        fields = '__all__'
        model = Employee
