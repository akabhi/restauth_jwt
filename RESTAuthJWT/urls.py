
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from testapp import views

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

router = routers.DefaultRouter()
'''
in case of ModelViewSet base_name is optional
router.register('api', views.EmployeeCRUDCBV, base_name='api')
'''
router.register('api', views.EmployeeCRUDCBV)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('auth-jwt/', obtain_jwt_token),
    path('auth-jwt-refresh/', refresh_jwt_token),
    path('auth-jwt-verify/', refresh_jwt_token),
]
